<?php

/*********************************************************************************
* LiveZilla config.php
*
* Copyright 2016 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
********************************************************************************/

$_CONFIG['gl_pr_cr'] = '1573058076';
$_CONFIG['gl_lzid'] = 'c20d80be88';
$_CONFIG['b64'] = false;

$_CONFIG[0]['gl_db_host'] = 'localhost/orcl';
$_CONFIG[0]['gl_db_user'] = 'livez';
$_CONFIG[0]['gl_db_ext'] = 'oci';
$_CONFIG[0]['gl_db_eng'] = 'oracle';
$_CONFIG[0]['gl_db_pass'] = 'lzilla';
$_CONFIG[0]['gl_db_name'] = 'livez';
$_CONFIG[0]['gl_db_prefix'] = 'LZ_';

?>